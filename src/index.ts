import "reflect-metadata";
import {createExpressServer} from "routing-controllers";

createExpressServer({
    controllers: [`${__dirname}/routes/*.js`],
    routePrefix: '/api/rest/v1'
}).listen(3000);
