import {Controller, Get} from "routing-controllers";

@Controller()
export class ExampleRoute {

    @Get('/example')
    sayHello(){
        return {info: "Some Example!"};
    }
}
